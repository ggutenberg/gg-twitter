<?php
/*
 * Plugin Name: Grinning Gecko Twitter
 * Version: 1.2
 * Plugin URI: http://grinninggecko.com/wordpress-gg-twitter-widget/
 * Description: Grinning Gecko Twitter Widget
 * Author: Garth Gutenberg
 * Author URI: http://grinninggecko.com/
 */
class GgTwitterWidget extends WP_Widget {

	public static $registration_url = 'http://dev.twitter.com/apps/new';

	/**
	* Declares the GgTwitterWidget class.
	*
	*/
	function __construct( $widget = true ) {
		if ( $widget ) {
			$widget_ops = array(
				'classname' => 'widget_gg_twitter',
				'description' => __( 'Grinning Gecko Twitter Widget' )
			 );
			$control_ops = array(
				'width' => 300,
				'height' => 300
			 );
			$this->WP_Widget( 'ggtwitter', __( 'GG Twitter' ), $widget_ops, $control_ops );

			add_option( 'ggtwitter_settings', array(
				'consumer_key' => null,
				'consumer_secret' => null,
				'access_token' => null,
				'access_token_secret' => null,
				'screen_name' => 'grinninggecko',
				'count' => 5,
				'include_rts' => true,
				'exclude_replies' => false
			) );

		}
	}

	function shortcode( $instance ) {
		ob_start( );
		$ggtwitter = new GgTwitterWidget( false );
		$ggtwitter->widget( array(
			'element' => 'div',
			'before_widget' => '',
			'after_widget' => ''
		 ), $instance );
		return ob_get_flush( );
		unset( $ggtwitter );
	}

	function load_file_from_url( $url ) {
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_REFERER, get_home_url() );
		$str = curl_exec( $curl );
		curl_close( $curl );
		return $str;
	}

	function load_xml_from_url( $url ) {
		return simplexml_load_string( $this->load_file_from_url( $url ) );
	}

	public function add_settings_page() {
		add_options_page(
			__( 'GG Twitter Settings', 'ggtwitter' ),
			__( 'GG Twitter', 'ggtwitter' ),
			'edit_plugins',
			'ggtwitter',
			array( 'GgTwitterWidget', 'settings_view' )
		);
	}

	public function add_settings_link($links) {
		array_unshift($links, '<a href="options-general.php?page=ggtwitter">Settings</a>');
		return $links;
	}

	public function validate_settings( $settings ) {
		foreach ( $settings as $index => $setting ) {
			if ( $setting === 'true' || $setting === 'false' )
				$settings[ $index ] = filter_var( $setting, FILTER_VALIDATE_BOOLEAN );
		}
		return $settings;
	}

	public function save_settings() {

		/** Bail if not our plugin page or not saving settings */
		if ( !isset( $_GET['page'] ) )
			return;
		if ( $_GET['page'] != 'ggtwitter' )
			return;
		if ( !isset( $_POST['settings'] ) )
			return;

		/** Security check. */
		if ( !check_admin_referer( "ggtwitter-save_{$_GET['page']}", "ggtwitter-save_{$_GET['page']}" ) ) {
			wp_die( __( 'Security check has failed. Save has been prevented. Please try again.', 'ggtwitter' ) );
			exit();
		}

		/** Save the settings */
		update_option( 'ggtwitter_settings', stripslashes_deep( self::validate_settings( $_POST['settings'] ) ) );

		/** Display success message */
		add_action( 'admin_notices', create_function( '', 'echo "<div class=\"message updated\"><p>'. __( 'Settings have been saved successfully.', 'ggtwitter' ) .'</p></div>";' ) );

	}

	public function settings_view() {

		/** Get the plugin settings */
		$settings = $s = self::validate_settings( get_option( 'ggtwitter_settings' ) );

		/** Print the view */
		?>
		<div class="wrap">
			<div id="icon-edit" class="icon32 icon32-posts-post"><br></div>
			<h2><?php _e( 'GG Twitter Configuration', 'ggtwitter' ); ?></h2>
			<form name="post" action="options-general.php?page=ggtwitter" method="post">
				<?php
				/** Security nonce field */
				wp_nonce_field( "ggtwitter-save_{$_GET['page']}", "ggtwitter-save_{$_GET['page']}", false );
				?>

				<div class="main-panel">
					<div class="section">
						<h3><?php _e( 'Authentication', 'ggtwitter' ); ?></h3>
						<p><?php _e( 'Twitter\'s v1.1 API requires authentication. For this you need to <a href="'. self::$registration_url .'">register an application here</a>. Follow the instructions and that\'s it, you\'re authenticated.', 'ggtwitter' ); ?></p>
						<table class="form-table settings">
							<tbody>
							<tr valign="top">
								<th scope="row"><label for="consumer_key"><?php _e( 'Consumer Key', 'ggtwitter' ); ?></label></th>
								<td>
									<input type="text" name="settings[consumer_key]" id="consumer_key" class="regular-text" value="<?php echo $s['consumer_key']; ?>">
									<p class="description"><?php _e( 'Enter your Consumer Key.', 'ggtwitter' ); ?></p>
								</td>
							</tr>

							<tr valign="top">
								<th scope="row"><label for="consumer_secret"><?php _e( 'Consumer Secret', 'ggtwitter' ); ?></label></th>
								<td>
									<input type="password" name="settings[consumer_secret]" id="consumer_secret" class="regular-text" value="<?php echo $s['consumer_secret']; ?>">
									<p class="description"><?php _e( 'Enter your Consumer Secret. Keep this private, do not share it.', 'ggtwitter' ); ?></p>
								</td>
							</tr>

							<tr valign="top">
								<th scope="row"><label for="access_token"><?php _e( 'Access Token', 'ggtwitter' ); ?></label></th>
								<td>
									<input type="text" name="settings[access_token]" id="access_token" class="regular-text" value="<?php echo $s['access_token']; ?>">
									<p class="description"><?php _e( 'Enter your Access Token.', 'ggtwitter' ); ?></p>
								</td>
							</tr>

							<tr valign="top">
								<th scope="row"><label for="access_token_secret"><?php _e( 'Access Token Secret', 'ggtwitter' ); ?></label></th>
								<td>
									<input type="password" name="settings[access_token_secret]" id="access_token_secret" class="regular-text" value="<?php echo $s['access_token_secret']; ?>">
									<p class="description"><?php _e( 'Enter your Access Token Secret. Keep this private also, do not share it.', 'ggtwitter' ); ?></p>
								</td>
							</tr>
							</tbody>
						</table>
					</div>

					<p class="submit">
						<input type="submit" name="save" class="button button-primary button-large" id="save" accesskey="p" value="<?php _e( 'Save Settings', 'ggtwitter' ); ?>">
					</p>
				</div>
			</form>
		</div>
	<?php

	}


	/**
	* Displays the Widget
	*
	* @todo Check if the current twitter handle is the same as the cached (transient, option) handle and refresh cache if different
	*/
	function widget( $args, $instance ) {
		extract( $args );

		if ( empty( $element ) )
			$element = 'div';

		if ( empty( $before_title ) )
			$before_title = '<h3>';

		if ( empty( $after_title ) )
			$after_title = '</h3>';

		if ( empty( $instance['title'] ) )
			$title = '';
		else
			$title = $instance['title'];

		if ( empty( $instance['username'] ) )
			$username = '';
		else
			$username = $instance['username'];

		if ( empty( $instance['max_tweets'] ) )
			$max_tweets = 3;
		else
			$max_tweets = ( int ) $instance['max_tweets'];

		if ( empty( $instance['view_all_html'] ) )
			$view_all_html = '?>View All Tweets';
		else
			$view_all_html = '?>' . $instance['view_all_html'];

		if ( empty( $instance['time_placement'] ) )
			$time_placement = 'before';
		else
			$time_placement = $instance['time_placement'];

		if ( empty( $instance['time_format'] ) )
			$time_format = 'D M jS';
		else
			$time_format = $instance['time_format'];

		if ( empty( $instance['cache_expiration'] ) )
			$cache_expiration = 10 * 60;
		else
			$cache_expiration = $instance['cache_expiration'] * 60;

		if ( empty( $instance['debug'] ) )
			$debug = false;
		else
			$debug = (bool) $instance['debug'];

		// Before the widget
		echo $before_widget;

		if ( $debug ) {
			$debug_output = '';
			$debug_output .= '<style>';
			$debug_output .= '.gg_twitter_debug{width:100%;overflow:scroll;background-color:white;color:black;font-family:monospace;text-shadow:none;font-size:10px;}' . "\n";
			$debug_output .= '</style>';
			$debug_output .= '<textarea class="gg_twitter_debug" readonly="readonly" rows="20" onclick="this.focus();this.select();">GG TWITTER DEBUG' . "\n\n";
			$debug_output .= 'WP Version: ' . get_bloginfo( 'version' ) . "\n\n";
			$debug_output .= 'PHP Version: ' . phpversion() . "\n\n";
			$debug_output .= 'GG Twitter Version: ' . $instance['version'] . "\n\n";
			if ( ! function_exists( 'simplexml_load_file' ) ) {
				$debug_output .= 'SimpleXML is unavailable.' . "\n\n";
			}
			$debug_output .= '$args:' . "\n";
			$debug_output .= htmlspecialchars(print_r($args, true)) . "\n\n";
			$debug_output .= '$instance:' . "\n";
			$debug_output .= htmlspecialchars(print_r($instance, true)) . "\n\n";
		}

//		delete_transient( $widget_id );
//		delete_option( $widget_id );

		// Get the transient
		$tweets = get_transient( $widget_id );
		if ( false === $tweets ) {

			echo 'No transient. Loading from API';

			$settings = $this->validate_settings( get_option( 'ggtwitter_settings' ) );

			/** Require the twitter auth class */
			if ( !class_exists('TwitterOAuth') )
				require_once 'includes/Twitter/twitteroauth/twitteroauth.php';

			/** Get Twitter connection */
			$twitterConnection = new TwitterOAuth(
				$settings['consumer_key'],
				$settings['consumer_secret'],
				$settings['access_token'],
				$settings['access_token_secret']
			);

			$tweets = $twitterConnection->get(
				'statuses/user_timeline',
				array(
					'screen_name' => $username,
					'count' => $max_tweets
				)
			);

//			echo '<pre>' . htmlspecialchars( print_r( $tweets, true ) ) . '</pre>';

			$json_xml = json_encode( $tweets );

			// Update the transient
			set_transient( $widget_id, $tweets, $cache_expiration );

			// Update the option
			update_option( $widget_id, $tweets );
		}

			///////////////////////

		?>
		<<?php echo $element; ?> class="gg-twitter widget-container">
			<?php if ( $title != '' ) { ?>
				<?php echo $before_title; ?><?php echo $title; ?><?php echo $after_title; ?>
			<?php } ?>
			<div class="tweets">
			<?php

			if ( $max_tweets > count ( $tweets ) )
				$max_tweets = count ( $tweets );

			for ( $i = 0; $i < $max_tweets; $i++ ) {

				$item = $tweets[$i];

				$tweeted_by = trim( $item->user->screen_name );
				$tweet_contents = trim( $item->text );
				$tweet_date = date( $time_format, strtotime( $item->created_at ) );

				// Parse/Link URLs
				$tweet_contents = preg_replace( '/http(\S+)/', '<a class="tweet-link" target="_blank" href="http$1">http$1</a>', $tweet_contents );
				// Parse/Link Email Addresses
				//$tweet_contents = preg_replace( '/\b([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})\b/', '<a class="tweet-email" href="mailto:$1">$1</a>', $tweet_contents );
				// Parse/Link Twitter Names
				$tweet_contents = preg_replace( '/@(\S+?)\b/', ' <a class="tweet-at" target="_blank" href="http://twitter.com/$1">@$1</a>', $tweet_contents );
				// Parse/Link Twitter Hashtags
				$tweet_contents = preg_replace( '/#(\S+?)\b/', ' <a class="tweet-hash" target="_blank" href="http://twitter.com/search?q=%23$1&amp;src=hash">#$1</a>', $tweet_contents );
				?>
				<div class="tweet<?php
				if ( $i == 0 ) {
					echo ' first';
				}
				if ( $i == ( $max_tweets - 1 ) ) {
					echo ' last';
				}
				?>">
					<?php if ( $time_placement == 'before' ) { ?>
						<div class="tweet-date"><?php echo $tweet_date; ?></div>
					<?php } ?>
					<div class="tweet-by"><a target="_blank" href="http://twitter.com/<?php echo $tweeted_by; ?>">@<?php echo $tweeted_by; ?></a></div>
					<div class="tweet-contents"><?php echo $tweet_contents; ?></div>
					<?php if ( $time_placement == 'after' ) { ?>
						<div class="tweet-date"><?php echo $tweet_date; ?></div>
					<?php } ?>
				</div>
				<?php
			}
			?>
			</div>
			<div class="view-all-tweets"><a target="_blank" href="http://twitter.com/<?php echo $username; ?>"><?php eval( $view_all_html ); ?></a></div>
		</<?php echo $element; ?>>
		<?php

		// After the widget
		echo $after_widget;
	}

	/**
	* Saves the widgets settings.
	*
	*/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes( $new_instance['title'] ) );
		$instance['username'] = strip_tags( stripslashes( $new_instance['username'] ) );
		$instance['max_tweets'] = strip_tags( stripslashes( $new_instance['max_tweets'] ) );
		$instance['view_all_html'] = $new_instance['view_all_html'];
		$instance['time_placement'] = $new_instance['time_placement'];
		$instance['time_format'] = $new_instance['time_format'];
		$instance['cache_expiration'] = $new_instance['cache_expiration'];
		$instance['debug'] = isset( $new_instance['debug'] ) ? 1 : 0;
		$plugin_data = get_plugin_data( __FILE__, false );
		$instance['version'] = $plugin_data['Version'];

		return $instance;
	}

	/**
	* Creates the edit form for the widget.
	*
	*/
	function form( $instance ) {
		$instance = wp_parse_args( ( array ) $instance, array(
			'title'=>'',
			'username'=>'',
			'max_tweets'=>'3',
			'view_all_html'=>'',
			'time_placement' => '',
			'time_format' => '',
			'cache_expiration' => 10, // minutes
			'debug' => 0
		 ) );

		$title = htmlspecialchars( $instance['title'] );
		$username = htmlspecialchars( $instance['username'] );
		$max_tweets = htmlspecialchars( $instance['max_tweets'] );
		$view_all_html = htmlspecialchars( $instance['view_all_html'] );
		$time_placement = htmlspecialchars( $instance['time_placement'] );
		$time_format = htmlspecialchars( $instance['time_format'] );
		$cache_expiration = (int) $instance['cache_expiration']; // minutes

		// Output the options
		?>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				Title:
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'title' ); ?>"
				name="<?php echo $this->get_field_name( 'title' ); ?>"
				type="text"
				value="<?php echo $title; ?>"
				/>
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'username' ); ?>">
				Twitter Username:
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'username' ); ?>"
				name="<?php echo $this->get_field_name( 'username' ); ?>"
				type="text"
				value="<?php echo $username; ?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'max_tweets' ); ?>">
				Max Tweets to Display:
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'max_tweets' ); ?>"
				name="<?php echo $this->get_field_name( 'max_tweets' ); ?>"
				type="text"
				value="<?php echo $max_tweets; ?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'view_all_html' ); ?>">
				HTML for the View All Link:
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'view_all_html' ); ?>"
				name="<?php echo $this->get_field_name( 'view_all_html' ); ?>"
				type="text"
				value="<?php echo $view_all_html; ?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'time_placement' ); ?>">
				Time Placement
			</label>
			<select
				class="widefat"
				id="<?php echo $this->get_field_id( 'time_placement' ); ?>"
				name="<?php echo $this->get_field_name( 'time_placement' ); ?>" >
				<option
					name="before"
					value="before"<?php echo ( $time_placement == 'before' ? ' selected="selected"' : '' ); ?>
					>Before</option>
				<option
					name="after"
					value="after"<?php echo ( $time_placement == 'after' ? ' selected="selected"' : '' ); ?>
					>After</option>
				<option
					name="none"
					value="none"<?php echo ( $time_placement == 'none' ? ' selected="selected"' : '' ); ?>
					>None</option>
			</select>
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'time_format' ); ?>">
				Time Format ( see <a href="http://php.net/manual/en/function.date.php" target="_blank">PHP: date</a> for reference ):
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'time_format' ); ?>"
				name="<?php echo $this->get_field_name( 'time_format' ); ?>"
				type="text"
				value="<?php echo $time_format; ?>" />
		</p>
		<p style="text-align:left;">
			<label for="<?php echo $this->get_field_id( 'cache_expiration' ); ?>">
				Cache Expiration (in minutes):
			</label>
			<input
				class="widefat"
				id="<?php echo $this->get_field_id( 'cache_expiration' ); ?>"
				name="<?php echo $this->get_field_name( 'cache_expiration' ); ?>"
				type="text"
				value="<?php echo $cache_expiration; ?>" />
		</p>
		<?php
	}

}// END class


/**
* Register widget.
*
* Calls 'widgets_init' action after the widget has been registered.
*/
function GgTwitterInit( ) {
	register_widget( 'GgTwitterWidget' );
}
add_action( 'widgets_init', 'GgTwitterInit' );
add_action( 'admin_menu', array( 'GgTwitterWidget', 'add_settings_page' ) );
add_action( 'admin_menu', array( 'GgTwitterWidget', 'save_settings' ) );
//add_filter( "plugin_action_links_{$plugin}", array( $this, 'add_settings_link' ) );


// Shortcode
add_shortcode( 'gg-twitter', array( 'GgTwitterWidget', 'shortcode' ) );

?>